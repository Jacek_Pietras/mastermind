/*jshint node: true */

var express = require('express');
var _ = require('underscore');
var bodyParser = require("body-parser");
var morgan = require("morgan");
var session = require("express-session");
var path = __dirname + "/public/";


var app = express();

app.use(morgan("dev"));
app.use(express.static('public'));
app.use(bodyParser.json());
app.use('/jquery', express.static('bower_components/jquery/dist'));
app.use("/bootstrap", express.static("bower_components/bootstrap/dist"));
app.use(session({
    secret: "mastermind game",
    resave: true,
    saveUninitialized: true,
    cookie: {
        maxAge: 1000
    }
}));

var gameOptions = {
    codeSize: 0,
    maxColors: 0,
    maxMoves: 0,
    secret: [],
    inserted: [],
    black: 0,
    miss: 0
};


app.get("/", function (req, res) {
    var sess = req.session;
    res.sendFile(path + "game.html");
});

app.post("/newgame", function (req, res, next) {

    if (gameOptions.hasOwnProperty("miss")) {
        gameOptions.miss = 0;
    }

    if (gameOptions.hasOwnProperty("secret")) {
        gameOptions.secret = [];
    }

    if (gameOptions.hasOwnProperty("black")) {
        gameOptions.black = 0;
    }

    if (gameOptions.hasOwnProperty("codeSize")) {
        gameOptions.codeSize = parseInt(req.body.codeSize, 10);
    }

    if (gameOptions.hasOwnProperty("maxColors")) {
        gameOptions.maxColors = parseInt(req.body.maxColors, 10);
    }
    if (gameOptions.hasOwnProperty("maxMoves"))
        gameOptions.maxMoves = parseInt(req.body.maxMoves, 10);

    for (var i = 0; i < gameOptions.codeSize; i++) {
        gameOptions.secret.push(Math.floor(Math.random() * gameOptions.maxColors) + 0);
    }

    console.log("Liczby do zgadnięcia: " + "[" + gameOptions.secret + "]");
    next();
});

app.post("/move", function (req, res) {
    if (gameOptions.hasOwnProperty("inserted")) {
        for (var i = 0; i < req.body.move.length; i++) {
            req.body.move[i] = parseInt(req.body.move[i], 10);
        }
        gameOptions.inserted = req.body.move;
    }
    //console.log(gameOptions);
    var mark = function (code, move) {
        var calcBlack = function () {
            return _.size(_.filter(code, function (value, index) {
                return value === move[index];
            }));
        };
        var calculateWhites = function () {
            return _.countBy(code, function (num) {
                return num;
            });
        };
        var whiteFinder = function (code, move) {
            var appearancesMap = calculateWhites(code);
            return function (element) {
                if (appearancesMap[element] > 0) {
                    appearancesMap[element]--;
                    return true;
                }
                return false;
            };
        };
        var calcWhite = function () {
            return _.filter(move, whiteFinder(code, move));
        };

        return {
            black: calcBlack(),
            white: calcWhite(code, move).length - calcBlack(code, move)
        };
    };
    gameOptions.black = mark(gameOptions.secret, gameOptions.inserted).black;

    if (gameOptions.black !== gameOptions.codeSize) {
        gameOptions.miss++;
    }

    if (gameOptions.miss === gameOptions.codeSize) {
        console.log("Niestety porażka!");
    }
    console.log(gameOptions);
    console.log(mark(gameOptions.secret, gameOptions.inserted));

    res.json(gameOptions);
});


var server = app.listen(3000, function () {

    var host = server.address().address;
    var port = server.address().port;

    console.log("Serwer działa na http://%s:%s", host, port);
});