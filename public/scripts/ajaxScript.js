/*jshint jquery: true*/

$().ready(function () {
    var recivedJSON;

    $("#graj").click(function () {
        var gameData = {
            codeSize: $("#codeSize").val(),
            maxColors: $("#maxColors").val(),
            maxMoves: $("#maxMoves").val()
        };

        $.ajax({
            url: "/newgame",
            type: 'POST',
            data: JSON.stringify(gameData),
            contentType: 'application/json; charset=UTF-8',
            dataType: 'json',
            async: false,
            success: function () {}
        });
        $("#codeSize").prop('disabled', true);
        $("#maxColors").prop('disabled', true);
        $("#maxMoves").prop('disabled', true);
        $("#graj").prop('disabled', true);

        for (var i = 0; i < gameData.codeSize; i++) {
            $("<input type='text' id='zgaduj' class='form-control'/>").insertAfter(".tuGeneruj");
        }

        $("#wyslijZgaduj").show();
    });

    $("#wyslijZgaduj").click(function () {
        var inserted = {
            move: []
        };

        $("#calePole input[id='zgaduj']").each(function () {
            inserted.move.push($(this).val());
        });

        $.ajax({
            url: '/move',
            type: 'POST',
            data: JSON.stringify(inserted),
            contentType: 'application/json; charset=UTF-8',
            dataType: 'json',
            async: false,
            success: function (serverData) {
                recivedJSON = serverData;
            }
        });

        if (recivedJSON.black === recivedJSON.codeSize) {
            $("<p>Udało ci się wygrać :)</p>").insertBefore(".tuGeneruj");
            $("#calePole input[id='zgaduj']").each(function () {
                $(this).prop('disabled', true);
            });
        }
        if (recivedJSON.miss === recivedJSON.maxMoves) {
            $("<p>Niestety porażka ;/</p>").insertBefore(".tuGeneruj");
            $("#calePole input[id='zgaduj']").each(function () {
                $(this).prop('disabled', true);
            });
        }
    });
});